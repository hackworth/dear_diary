# coding=utf-8
"""Locate a diary file"""
import argparse
import datetime
import mmap

def locate_date(diary_file, when=None):
    """Find the offset into the file where the new entry should be added.
    If the date """
    if when is None:
        when = datetime.date.today()
    when_label = when.strftime("## %A %B %d, %Y\n")

    with open(diary_file, "rb") as diary_handle:
        diary_memory = mmap.mmap(diary_handle.fileno(), 0)
        file_index = find(when_label.encode())

def prepare_arguments() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("--diary_path", help="Directory holding the diaries",
                        default="~/Documents/diaries")

    parser.add_argument("diary", default="diary", nargs='?',
                        help="Base name for the diary.")
    return parser


def main():
    parser = prepare_arguments()
    args = parser.parse_args()
    print(args)


if __name__ == "__main__":
    main()
